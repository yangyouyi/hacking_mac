# 一、主要链接

##  OpenCore Install Guide:OC安装向导

https://dortania.github.io/OpenCore-Install-Guide/



## ACPI入门：acpi入门向导

https://dortania.github.io/Getting-Started-With-ACPI/



# 二、重要文件

## OpenCore

https://github.com/acidanthera/OpenCorePkg/releases

## HfsPlus.efi

https://github.com/acidanthera/OcBinaryData/blob/master/Drivers/HfsPlus.efi

## 1、必备Kexts

VirtualSMC
https://github.com/acidanthera/VirtualSMC/releases

Lilu
https://github.com/acidanthera/Lilu/releases

WhateverGreen
https://github.com/acidanthera/WhateverGreen/releases

## 2、常用Kexts

### USBInjectAll

usb
https://bitbucket.org/RehabMan/os-x-usb-inject-all/downloads/

### AppleALC

声卡
https://github.com/acidanthera/AppleALC/releases

### VoodooHDA

“万能声卡”
https://sourceforge.net/projects/voodoohda/

### Wireless-USB-OC-Big-Sur-Adapter

万能usb无线网卡
https://github.com/chris1111/Wireless-USB-OC-Big-Sur-Adapter

### 网卡

IntelMausi
对于大多数Intel NIC都是必需的，基于I211的芯片组将需要SmallTreeIntel82576 kext
正式支持英特尔的82578、82579，i217，i218和i219 NIC
需要OS X 10.9或更高版本，10.8-10.8用户可以使用IntelSnowMausi代替旧的OS
https://github.com/acidanthera/IntelMausi/releases
SmallTreeIntel82576 kext
i211 NIC必需，基于SmallTree kext，但已打补丁以支持I211
大多数运行Intel NIC的AMD板必需
需要OS X 10.9-12（v1.0.6），macOS 10.13-14（v1.2.5），macOS 10.15+（v1.3.0）
https://github.com/khronokernel/SmallTree-I211-AT-patch/releases
AtherosE2200Ethernet
Atheros和Killer NIC需要
需要OS X 10.8或更高版本
注：Atheros的杀手E2500模型实际上是基于瑞昱，对于这些系统，请使用RealtekRTL8111代替
https://github.com/Mieze/AtherosE2200Ethernet/releases
RealtekRTL8111
对于Realtek的千兆以太网
要求OS X 10.8-11（2.2.0），10.12-13（v2.2.2），10.14 +（2.3.0）
注意：如果您拥有RealtekRTL8111 v2.3.0，有时Realtek的千兆以太网可能无法正常工作。如果您看到此问题，请尝试还原到2.2.2版
https://github.com/Mieze/RTL8111_driver_for_OS_X/releases



### LucyRTL8125Ethernet

对于Realtek的2.5Gb以太网
需要macOS 10.15或更高版本
https://www.insanelymac.com/forum/files/file/1004-lucyrtl8125ethernet/
XHCI-unsupported
非本机USB控制器需要
https://github.com/RehabMan/OS-X-USB-Inject-All

### WiFi and Bluetooth

Intel

AirportItlwm
借助IO80211Family集成，增加了对多种Intel无线网卡的支持，并可以在恢复中正常运行
需要macOS 10.13或更高版本，并且需要Apple的安全启动才能正常运行
https://github.com/OpenIntelWireless/itlwm/releases
IntelBluetoothFirmware
与Intel无线网卡配对时为macOS添加蓝牙支持
需要macOS 10.13或更高版本
https://github.com/OpenIntelWireless/IntelBluetoothFirmware/releases
Broadcom

AirportBrcmFixup
用于修补非Apple /非Fenvi Broadcom卡，不适用于Intel，Killer，Realtek等
需要OS X 10.10或更高版本
https://github.com/acidanthera/AirportBrcmFixup/releases
BrcmPatchRAM
用于在所有非Apple /非芬维机场卡上将固件上传到Broadcom蓝牙芯片组上。
与BrcmFirmwareData.kext配对
https://github.com/acidanthera/BrcmPatchRAM/releases
笔记本电脑
VoodooPS2
对于带有PS2键盘，鼠标和触控板的系统
需要MT2（Magic Trackpad 2）功能的macOS 10.11或更高版本
https://github.com/acidanthera/VoodooPS2/releases
RehabMan’s VoodooPS2
对于带有PS2键盘，鼠标和触控板的较早系统，或者当您不想使用VoodooInput时
支持macOS 10.6+支持
https://bitbucket.org/RehabMan/os-x-voodoo-ps2-controller/downloads/
VoodooRMI
对于具有基于Synaptics SMBus的设备的系统，主要用于触控板和跟踪点。
MT2功能需要macOS 10.11或更高版本
https://github.com/VoodooSMBus/VoodooRMI/releases/
VoodooSMBus
对于具有基于ELAN SMBus的设备的系统，主要用于触控板和跟踪点。
当前支持macOS 10.14或更高版本
https://github.com/VoodooSMBus/VoodooSMBus/releases
VoodooI2C
用于固定I2C设备，某些高级触摸板和触摸屏机器随附
MT2功能需要macOS 10.11或更高版本
https://github.com/VoodooI2C/VoodooI2C/releases

# 三、工具软件

ProperTree
https://github.com/corpnewt/ProperTree

python
https://www.python.org/downloads/

opencore-configurator
https://mackie100projects.altervista.org/opencore-configurator/

Opencore Gen-X
https://mackie100projects.altervista.org/opencore-configurator/ 好像错误了

https://mac.orsoon.com/Mac/182107.html



DPCIManager
https://github.com/MuntashirAkon/DPCIManager/releases

Hackintool
https://github.com/headkaze/Hackintool/releases

预建的SSDT（Pre-Built SSDTs）
https://dortania.github.io/Getting-Started-With-ACPI/ssdt-methods/ssdt-prebuilt.html#intel-desktop-ssdts

OpenCore beauty treatment（启动菜单图形界面主题）
https://dortania.github.io/OpenCore-Post-Install/cosmetic/gui.html#setting-up-opencore-s-gui




# 四、重点关注的kext地址

```py
我的ax200也驱动上了,但是不管是2.4还是5g，链接速率都是144，不知道原因为何？

这款卡就是这样，需要从Win重启才能正常用蓝牙，目前无解，要么换博通的卡


http://bbs.pcbeta.com/forum.php? ... t=intel%C0%B6%D1%C0
蓝牙需进windows才生效解决办法


ax200要是能解决驱动就太爽了

已解决

Wi-Fi
https://github.com/zxystd/itlwm

蓝牙
https://github.com/zxystd/IntelBluetoothFirmware




我的一切正常啊 论坛上两款驱动都没有问题 网速不知道多少 但可以使用，蓝牙IntelBluetoothFirmware.kext
```

