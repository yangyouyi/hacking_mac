# 一、配置
1. | 序号 | 类型    | 产品               | 规格               |
   | ---- | ------- | ------------------ | ------------------ |
   | 1    | 板U套装 | I5 10500+微星B460I GAMING EDGE WIFI刀锋板 | 双M2口             |
   | 2    | 散热器  | 利民AXP90I         | 支持10代因特尔CPU  |
   | 3    | 内存    | 光威 奕pro         | 2666MHz 16G 带马甲 |
   | 4    | 显卡    | 肥猫家黑苹果显卡   | RX560 4G           |
   | 5    | 硬盘    | 光威 奕pro         | nvme接口 SSD 512G  |
   | 6    | 机箱    | 乔思伯T8           | 红色               |
   | 7    | 电源    | 全汉ms450W         | 450W flex小电源1   |

# 二、需要注意的参数

## 声卡

瑞昱® ALC1200 解码芯片 7.1声道高音质输出

| 厂商 | **声卡芯片** | 修订/layoutID | 兼容最旧的内核版本 | 兼容最新的内核版本 |
| ---- | ------------ | ------------- | ------------------ | ------------------ |
| Realtek | [ALC888/ALC1200](https://github.com/vit9696/AppleALC/tree/master/Resources/ALC888) | 0x100101, 0x100001, 0x100202 layout 1, 2, 3, 5, 7 | 13 (10.9) |-|

---------------

集成Realtek ALC1200 7.1声道音效芯片

https://github.com/acidanthera/AppleALC/wiki/Supported-codecs

[ALC888/ALC1200](https://github.com/vit9696/AppleALC/tree/master/Resources/ALC888)

0x100101, 0x100001, 0x100202, 0x100302 layout 1, 2, 3, 4, 5, 7, 11, 27, 28, 29

13 (10.9)



## 网卡

无线网络适配器  	Intel(R) Wi-Fi 6 AX200 160MHz

有线网络适配器      2500M 瑞昱®RTL8125B 2.5G LAN

## USB

定制USB时，总数不能超过15个

## 蓝牙

![image-20210417163002907](README_res/image-20210417163002907.png)

# 三、照片



![image-20210321233718093](README_res/image-20210321233718093.png)





![image-20210417163306345](README.assets/image-20210417163306345.png)

# 四、说明

目前这几个EFI尚未完美，只是能够安装好，还需要后续修改EFI配置





# 五、必要的链接

## *检验配置的参数是否正确

https://opencore.slowgeek.com/



## winmd5：在win下验证MD5工具

也有linux版的

https://www.winmd5.com/

## Etcher：U盘写入镜像工具

https://www.balena.io/etcher/



## OpenCore下载和使用向导

https://dortania.github.io/OpenCore-Install-Guide/



## OpenCore补丁下载

https://dortania.github.io/Getting-Started-With-ACPI/



# OpenCore制作EFI常用下载地址集合

https://blog.csdn.net/lxyoucan/article/details/111056622





## ProperTree：OC配置编辑器

跨平台工具，只要有python环境

https://github.com/corpnewt/ProperTree

https://github.com/corpnewt/ProperTree.git





## acpigihub

https://github.com/dortania/Getting-Started-With-ACPI





# 六、ACPI知识、EFI知识

https://github.com/daliansky/OC-little.git